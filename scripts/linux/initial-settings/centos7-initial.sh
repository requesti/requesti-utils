#!/bin/bash

### (ami-61bbf104)
### SCRIPT UTILIZADO PARA CONFIGURAÇÃO INICIAL/ESSENCIAL NO SO CENTOS 7

### DEFININDO PROGRAMAS QUE O SCRIPT PODERA INSTALAR
PROGRAMS="bash-completion bind-utils bzip2 elinks expect gcc gcc-c++ git mariadb mlocate perl-IO-Socket-SSL perl-Net-SSLeay tcpdump telnet unzip vim wget zip jq"

### DEFININDO PASTAS PADRAO
FOLDERS="/apps /backups /downloads /git /teste /scripts/tools"

clear
echo "Este script pode redefinir todas as configuracoes deste servidor."
echo -ne "Tem certeza que deseja executa-lo? "
echo -ne "[y/N]: " ; read CONFIRMA_INICIO

if [ "$CONFIRMA_INICIO" = n ] || [ "$CONFIRMA_INICIO" = N ] || [ -z "$CONFIRMA_INICIO" ]; then
echo "Cancelando..."
exit
fi

if [ "$CONFIRMA_INICIO" != y ]; then
echo "Digite uma opcao valida!"
./$0
exit
fi

echo -ne "Deseja alterar o hostname do servidor? "
echo -ne "[y/N]: " ; read CONFIRMA_HOSTNAME
if [ "$CONFIRMA_HOSTNAME" = y ]; then
echo -ne "Digite o nome completo (nome e domínio) do servidor: (Ex: servidor1.requesti.corp) (DEIXE EM BRANCO PARA NÃO ALTERAR): " ; read COMPUTERNAME
if [ -z "$COMPUTERNAME" ]; then
echo "E necessario digitar um nome completo para o servidor. Como nao foi digitado, o nome do computador nao sera alterado."
COMPUTERNAME=$(hostname)
else
echo "O nome do servidor sera alterado para: $COMPUTERNAME"
fi
fi

echo -ne "Deseja alterar configuracoes do SSH? "
echo -ne "[y/N]: " ; read CONFIRMA_SSHPORT
if [ "$CONFIRMA_SSHPORT" = y ]; then
echo -ne "Digite a nova porta SSH (Padrao 22222): " ; read SSHPORT
if [ -z "$SSHPORT" ] || [ "$SSHPORT" -ge 65535 ]; then
echo "ATENCAO: porta definida sera a 22222 pois o numero digitado esta nulo ou maior que 65535."
SSHPORT="22222"
fi
fi

echo -ne "Deseja criar um novo usuario root personalizado? "
echo -ne "[y/N]: " ; read CONFIRMA_MODUSER
if [ "$CONFIRMA_MODUSER" = y ]; then
echo -ne "Digite o novo nome de usuario (Padrao root): " ; read MODUSER
if [ -z "$MODUSER" ]; then
echo "Como nao foi digitado nome de usuario, sera utilizado \"root\"."
MODUSER=root
fi
echo -ne "Digite a nova senha para o usuario "$MODUSER": " ; read MODPASS
if [ -z "$MODPASS" ] ; then
if [ "$MODUSER" = root ]; then
echo "Nao e possivel definir senha em branco para usuario root, cancelando..."
exit
fi
echo "ATENCAO: senha sera definida EM BRANCO!!! Cancele o script caso esteja errado (ctrl+c)."
fi
fi

echo -ne "Desabilitar o SELinux? "
echo -ne "[y/N]: " ; read CONFIRMA_SELINUX

echo -ne "Sincronizar hora de acordo com horario brasileiro (GMT -3:00)? "
echo -ne "[y/N]: " ; read CONFIRMA_HORA

echo -ne "Instalar repositorios EPEL, REMI e Webstatic? "
echo -ne "[y/N]: " ; read CONFIRMA_REPO

echo -ne "Atualizar servidor Linux (yum update)? "
echo -ne "[y/N]: " ; read CONFIRMA_UPDATE

echo -ne "Instalar programas uteis ("$PROGRAMS")? "
echo -ne "[y/N]: " ; read CONFIRMA_PROGRAMS

echo -ne "Adicionar aliases uteis? (l=ls -lha, por exemplo) "
echo -ne "[y/N]: " ; read CONFIRMA_ALIASES

echo -ne "Criar pastas padrao? ("$FOLDERS") "
echo -ne "[y/N]: " ; read CONFIRMA_FOLDERS

echo -ne "Instalar AWS CLI e EB CLI? "
echo -ne "[y/N]: " ; read CONFIRMA_AWSCLI

echo -ne "Alterar limites de arquivos abertos do CentOS? "
echo -ne "[y/N]: " ; read CONFIRMA_LIMITS

echo -ne "Rebootar servidor apos todas as alteracoes confirmadas? "
echo -ne "[y/N]: " ; read CONFIRMA_REBOOT

#############################################################################################

clear

### ALTERANDO HOSTNAME DO SERVIDOR
if [ "$CONFIRMA_HOSTNAME" = y ]; then
echo "$COMPUTERNAME" > /etc/hostname
PRES_HOST_ON=$(cat /etc/cloud/cloud.cfg | grep preserve_hostname)
if [ -z "$PRES_HOST_ON" ]; then
echo preserve_hostname: true >> /etc/cloud/cloud.cfg
fi
echo "HOSTNAME ALTERADO"
echo
fi

### ALTERANDO CONFIGURACOES DO SERVIDOR SSH
if [ "$CONFIRMA_SSHPORT" = y ]; then
semanage port -a -t ssh_port_t -p tcp $SSHPORT
sed -i "/^Port /d" /etc/ssh/sshd_config
sed -i -e '1i\' -e "Port $SSHPORT" /etc/ssh/sshd_config
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
systemctl restart sshd
echo "CONFIGURACOES SSH EFETUADAS"
echo
fi

### ALTERANDO CONFIGURACOES DE USUARIO ROOT E SENHA
if [ "$CONFIRMA_MODUSER" = y ]; then
echo "AllowUsers $MODUSER" >> /etc/ssh/sshd_config
if [ "$MODUSER" != root ]; then
useradd "$MODUSER"
fi
echo "$MODPASS" | passwd "$MODUSER" --stdin
if [ "$MODUSER" != root ]; then
sed -i "/^$MODUSER/d" /etc/passwd
sed -i -e '2i\' -e "$MODUSER:x:0:0::\/home\/$MODUSER:\/bin\/bash" /etc/passwd
sed -i "/^$MODUSER/d" /etc/group
sed -i -e '2i\' -e "$MODUSER:x:0:" /etc/group
chown -R $MODUSER:$MODUSER /home/$MODUSER
fi
systemctl restart sshd
echo "USUARIOS CRIADOS"
echo
fi

### DESABILITAR SELINUX
if [ "$CONFIRMA_SELINUX" = y ]; then
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
echo "SELINUX DESABILITADO"
echo
fi

### ALTERANDO FUSO PARA BRT
if [ "$CONFIRMA_HORA" = y ]; then
rm -rf /etc/localtime
ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
yum install ntp ntpdate -y
ntpdate br.pool.ntp.org
date
systemctl disable chronyd.service
systemctl enable ntpd.service
echo "DATA E HORA ALTERADA"
echo
fi

### INSTALANDO REPOSITORIOS
if [ "$CONFIRMA_REPO" = y ]; then
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
echo "REPOSITORIOS INSTALADOS"
echo
fi

### ATUALIZAR LINUX
if [ "$CONFIRMA_UPDATE" = y ]; then
yum update -y
yum clean all
yum history sync
yum repolist
echo "LINUX ATUALIZADO"
echo
fi

### INSTALANDO PROGRAMAS UTEIS
if [ "$CONFIRMA_PROGRAMS" = y ]; then
yum install $PROGRAMS -y
updatedb
echo "INSTALACAO DE PROGRAMAS EFETUADA"
echo
fi

### ADICIONANDO ALIAS UTEIS
if [ "$CONFIRMA_ALIASES" = y ]; then
echo 'alias vi="vim"' >> /etc/profile
echo 'alias l="ls -lha --color"' >> /etc/profile
echo 'alias c="clear"' >> /etc/profile
echo 'PATH=$PATH:/scripts' >> /etc/profile
echo "ALIASES ADICIONADOS"
echo
fi

### CRIANDO PASTAS PADRAO
if [ "$CONFIRMA_FOLDERS" = y ]; then
mkdir -p $FOLDERS
echo "PASTAS PADRAO CRIADAS"
echo
fi

### INSTALANDO AWS CLI
if [ "$CONFIRMA_AWSCLI" = y ]; then
DIR_DOWNLOAD="/downloads/awscli"
if [ ! -d "DIR_DOWNLOAD" ]; then
mkdir -p "$DIR_DOWNLOAD"
fi
yum install python-pip python-devel libevent-devel -y
cd "$DIR_DOWNLOAD" ; wget https://bootstrap.pypa.io/ez_setup.py --no-check-certificate
sleep 2; python ez_setup.py --insecure
wget https://bootstrap.pypa.io/get-pip.py --no-check-certificate
sleep 2 ; python get-pip.py
sleep 2 ; pip install --upgrade awscli
sleep 2 ; pip install --upgrade awsebcli
echo "AWS CLI INSTALADO"
echo
fi

### ALTERANDO LIMITS DE ARQUIVOS ABERTOS
if [ "$CONFIRMA_LIMITS" = y ]; then
sed -i 's/4096/307200/g' /etc/security/limits.d/20-nproc.conf
echo '* hard nofile 500000' >> /etc/security/limits.conf
echo '* soft nofile 500000' >> /etc/security/limits.conf
echo 'root hard nofile 500000' >> /etc/security/limits.conf
echo 'root soft nofile 500000' >> /etc/security/limits.conf
echo 'fs.file-max = 2097152' >> /etc/sysctl.conf
sleep 1; sysctl -p
echo "LIMITES DE ARQUIVOS ABERTOS ALTERADOS"
echo
fi

### INSTALAÇÃO CONCLUÍDA - REBOOT DO SERVER
if [ "$CONFIRMA_REBOOT" = y ]; then
reboot
fi
