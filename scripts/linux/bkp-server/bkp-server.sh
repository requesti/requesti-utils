#!/bin/bash

###########################################
### VARIAVEIS #############################
###########################################

### DIRETORIOS A SEREM BACKUPEADOS COMPLETAMENTE
DIRS_BKP_FULL="/downloads /etc /scripts"
### ATIVA BKP RECURSIVO? (PASTA POR PASTA, ARQUIVO POR ARQUIVO) (1=sim, 0=nao)
BKP_RECURS="0"
### DIRETORIOS A SEREM BACKUPEADOS RECURSIVAMENTE (PASTA POR PASTA, ARQUIVO POR ARQUIVO)
DIRS_BKP_RECURS=""
### DIRETORIO DE DESTINO DO BKP
BKP_DIR="/backups/dirs/$(hostname)"
### TIPO DE COMPRESSAO DO BKP (z=gzip, j=bzip) z=mais rapido e comprime menos, j=mais lento e comprime mais)
#COMPRESS="z"
COMPRESS="j"
### ENVIA EMAIL CASO FALHE? (1=sim, 0=nao)
ENVIA_EMAIL="0"
### EFETUA RECICLAGEM DE BKPS? (1=sim, 0=nao)
RECICLA_BKP="1"
### QUANTIDADE DE DIAS DE RETENCAO DOS BKPS
BKP_RETENTION="7"
### DIRETORIO PARA CRIACAO DE ARQUIVOS TEMPORARIOS
TEMP_DIR="/tmp/bkp_dirs"
### ARQUIVO ONDE SERA GRAVADO OS LOGS E TODAS A SAIDAS
LOGFILE="/var/log/bkp-dirs.log"
### TAMANHO MAXIMO DO ARQUIVO DE LOG (EM BYTES)
MAX_LOG="104857600" ### 100Mb

#######################################################################
############### CONFIGURACOES PARA ENVIO DE E-MAIL ####################
#######################################################################

EMAIL_TO="scripts@requesti.com.br"
EMAIL_FROM="scripts@requesti.com.br"
EMAIL_AUTH_USER="AKIAIETLA25HADFKT6MQ"
EMAIL_AUTH_PASS="AkFvK1WQ4Dj+0VIEwyVqGr1k2rJpLa1c5jAWwLgPNYwy"
EMAIL_SERVER="email-smtp.us-east-1.amazonaws.com:587"
USE_TLS="yes" ### yes OR no OR auto

###########################################
### INICIO DO SCRIPT ######################
###########################################

### ROTACIONANDO LOGS
if [ -e $LOGFILE ]; then
if [ "$(ls -la "$LOGFILE" | cut -d" " -f5)" -ge "$MAX_LOG" ]; then
tail -100000 $LOGFILE > "$LOGFILE"2
cat "$LOGFILE"2 > $LOGFILE
rm -rf "$LOGFILE"2
fi
fi

### REMOVENDO DIRETORIO TEMPORARIO
rm -rf "$TEMP_DIR"

### REDIRECIONANDO SAIDAS PARA O ARQUIVO DE LOG
exec &>> $LOGFILE

echo "#############################################################"
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Inicio do Script --"

### VALIDANDO VARIAVEL COMPRESS PARA DEFINIR TIPO DE COMPRESSAO
if [ "$COMPRESS" != "z" ] && [ "$COMPRESS" != "j" ]; then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Variavel '\$COMPRESS' invalida! Esta como: $COMPRESS, mas deve ser z ou j --"
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Cancelando execucao --"
exit
fi

if [ "$COMPRESS" = "z" ]; then
EXTENSION="gz"
fi

if [ "$COMPRESS" = "j" ]; then
EXTENSION="bz2"
fi

if [ ! -d "$BKP_DIR" ]; then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Criando diretorio de backup "$BKP_DIR", pois nao existe --"
mkdir -p "$BKP_DIR"
fi

if [ ! -d "$TEMP_DIR" ]; then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Criando diretorio temporario "$TEMP_DIR", pois nao existe --"
mkdir -p "$TEMP_DIR"
fi

for DIR in $DIRS_BKP_FULL; do
	echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Iniciando backup completo do diretorio "$DIR" --"
	echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Tamanho: $(du -sh "$DIR" | awk -F" " '{print $1}') --"
	DIR_NAME=$(echo $DIR | sed 's/\//-/g')
	tar "$COMPRESS"cfP $BKP_DIR/$(date +%Y%m%d)$DIR_NAME.tar.$EXTENSION $DIR
	if [ $? -ne 0 ]; then
	echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Erro ao fazer backup do diretorio "$DIR" --"
	if [ "$ENVIA_EMAIL" = 1 ]; then
	echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Enviando e-mail ao destino: "$EMAIL_TO" --"
	/usr/bin/sendEmail -u "[$(hostname)] Script: $0" -t $EMAIL_TO -s $EMAIL_SERVER -xu $EMAIL_AUTH_USER -xp $EMAIL_AUTH_PASS -f $EMAIL_FROM -o tls=$USE_TLS -m "[$(hostname)
] \nSou script $0, executado no servidor $(hostname). Desta vez, Tive um problema de execucao. Por favor, verificar nos logs o que pode ter acontecido. \n\nPrevia do lo
g: \n $(tail -20 $LOGFILE)"
fi
	else
	echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Backup do diretorio "$DIR" concluido com sucesso --"
	fi
done

if [ "$BKP_RECURS" = 1 ]; then
for DIR in $DIRS_BKP_RECURS; do
	for DIR_RECURS in $(ls $DIR); do
        echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Iniciando backup recursivo do diretorio "$DIR/$DIR_RECURS" --"
        echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Tamanho: $(du -sh "$DIR/$DIR_RECURS" | awk -F" " '{print $1}') --"
        DIR_NAME=$(echo $DIR | sed 's/\//-/g')
	tar "$COMPRESS"cfP $BKP_DIR/$(date +%Y%m%d)$DIR_NAME-$DIR_RECURS.tar.$EXTENSION $DIR/$DIR_RECURS
	if [ $? -ne "0" ]; then
        echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Erro ao fazer backup do diretorio "$DIR_RECURS" --"
        if [ "$ENVIA_EMAIL" = 1 ]; then
        echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Enviando e-mail ao destino: "$EMAIL_TO" --"
        /usr/bin/sendEmail -u "[$(hostname)] Script: $0" -t $EMAIL_TO -s $EMAIL_SERVER -xu $EMAIL_AUTH_USER -xp $EMAIL_AUTH_PASS -f $EMAIL_FROM -o tls=$USE_TLS -m "[$(hostname)] \nSou script $0, executado no servidor $(hostname). Desta vez, Tive um problema de execucao. Por favor, verificar nos logs o que pode ter acontecido. \n\nPrevia do log: \n $(tail -20 $LOGFILE)"
fi
        else
        echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Backup do diretorio "$DIR" concluido com sucesso --"
        fi
done
done
fi

rm -rf $TEMP_DIR

echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Fim do script --"

#######################################################################
##################### INICIO DA RECICLAGEM ############################
#######################################################################

if [ "$RECICLA_BKP" = 1 ]; then

cd $BKP_DIR

echo "Iniciando Reciclagem..."
echo "Arquivos removidos: "
find -mtime +$BKP_RETENTION -type f -exec ls -lha {} \; | awk -F" " '{print $NF}'
find -mtime +$BKP_RETENTION -type f -exec rm -rf {} \;
else
echo "Pulando reciclagem dos backups, script definido para nao fazer reciclagem."
fi
