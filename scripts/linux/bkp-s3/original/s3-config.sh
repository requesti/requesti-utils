#!/bin/bash

###
# Script: s3-bkp.sh v.1.7
###

# Nome do servidor que o backup esta sendo executado
SRV=servidor.meudominio.com.br

# Onde os arquivos temporarios serao compactados
TEMP=/tmp/bkp-$SRV

# Ativa timeout nos comandos de upload?
TIMEOUT=0
# Caso o timeout esteja ativo, quantos segundos esperar antes de cancelar comando?
TIMEOUT_TIME=300


#==================================================#
# BKP_MYSQL : 
# --- Faz backup do MySQL?
#==================================================#
BKP_MYSQL=0

MYSQL_USER=root
MYSQL_PASS=XXXXXX
MYSQL_HOSTS='localhost'


#==================================================#
# DIR_BACKUP : 
# --- Backup de diretorio (nao recursivo)
#==================================================#
DIR_BACKUP="/etc /scripts"


#==================================================#
# DIR_RECURSIVO : 
# --- faz um for dentro de cada 
# dir abaixo, e gera um tar de cada subdiretorio
#==================================================#
BKP_RECURSIVO=0
BKP_RECURSIVO_DIRS="/teste /teste2 "

# onde os logs do scripts de backup ficarao?
SCRIPT_LOG=/var/log/s3-bkp-server.log

# Onde esta o cli da AWS
# CentOS 
AWS_CLI=/usr/bin/aws
# Ubuntu 
#AWS_CLI=/usr/local/bin/aws

#Configuracoes do TAR
TAR_CFG="-zcf"
TAR_EXT="tar.gz"
TAR_EXTRACT="zxf"

# Credenciais da AWS
# 1. OPCAO: Usando IAM ROLE
 export AWS_DEFAULT_REGION=us-east-1
# 2. OPCAO: Usando credenciais no arquivo
#export AWS_CONFIG_FILE=/scripts/s3-bkp/aws-cred.conf

# Envia emails de erros?
ENVIA_EMAIL=1

EMAIL_TO="backupmon@brlink.com.br"
EMAIL_FROM="backupmon@brlink.com.br"
EMAIL_SERVER="email-smtp.us-east-1.amazonaws.com:587"

#### INSIRA AS CREDENCIAIS GERADAS PELO SES 
EMAIL_AUTH_USER=" "
EMAIL_AUTH_PASS=" "
