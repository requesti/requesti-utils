#!/bin/bash

CONFIG=/scripts/s3-bkp/s3-config.sh

if [ -f $CONFIG ]; then
	. $CONFIG
else
    echo "s3-bkp-server.sh: Arquivo de configuracoes $CONFIG nao encontrado! "
	exit 1
fi

if [ $TIMEOUT == 1 ]; then
   timeout="timeout $TIMEOUT_TIME"
else
   timeout=""      
fi


DRY=0
if [ "$2" == "--dry" ]; then
   DRY=1
   DRY_TEMP=$TEMP/dry-output.txt
   DRY_OUTPUT=/tmp/s3-bkp-dry-output.txt
   DRY_CHECK_TEMP=$TEMP/dry-check-output.txt
   DRY_CHECK_OUTPUT=/tmp/s3-bkp-dry-check-output.txt
fi


function valida_arq_no_s3 {
   ERR_VALIDA=1
   CHECK_S3=`$timeout $AWS_CLI s3 ls s3://$1/$2`
   VALIDA=`/bin/echo $CHECK_S3 | grep $2`
   if [ "$VALIDA" ] ; then
     ERR_VALIDA=0
   fi
}
   
function upload_to_s3 {
    #Subindo arquivo para o S3
    $timeout $AWS_CLI s3 cp $TEMP/$2 s3://$1/

    valida_arq_no_s3 $1 $2
    if [ $ERR_VALIDA == 1 ] ; then 
       # Tenta subir uma segunda vez
       $timeout $AWS_CLI s3 cp $TEMP/$2 s3://$1/
       valida_arq_no_s3 $1 $2 
       if [ $ERR_VALIDA == 1 ] ; then 
          # Tenta subir uma terceira vez
          $timeout $AWS_CLI s3 cp $TEMP/$2 s3://$1/
          valida_arq_no_s3 $1 $2
          if [ $ERR_VALIDA == 1 ] ; then 
	     /bin/echo `date "+%b %d %H:%M:%S "`"s3-bkp-server.sh : ERRO: nao foi posssivel subir o arquivo $2 no S3, solicitei para enviar email de erro ao fim do script!" >> $SCRIPT_LOG;
             /bin/rm -f $TEMP/$2 > /dev/null 2> /dev/null
             ERRO=1
          else
             LOG_OK=1
          fi
       else
          LOG_OK=1
       fi
    else
       LOG_OK=1
    fi   
    
    if [ $LOG_OK == 1 ]; then
       /bin/echo `date "+%b %d %H:%M:%S "`"s3-bkp-server.sh : Arquivo $2 carregado com sucesso no S3" >> $SCRIPT_LOG;
       /bin/rm -f $TEMP/$2 > /dev/null 2> /dev/null
    fi
    

}


if [ "$1" ]; then


	ERRO=0

	# Limpando Temps
	rm -rf $TEMP
	mkdir -p $TEMP
	/bin/echo `date "+%b %d %H:%M:%S "`"s3-bkp-server.sh : Iniciando backup do servidor $SRV" >> $SCRIPT_LOG

	########
	# MYSQL
	########
	if [ $BKP_MYSQL -gt 0 ] ; then
               for dbhost in $MYSQL_HOSTS; do
		  #Baixando lista de bases do host
	     	  mysql -u $MYSQL_USER -p"$MYSQL_PASS" -h $dbhost -e 'SHOW DATABASES;' | cut -d'|' -f2 | grep -v Database | grep -v innodb | grep -v tmp | grep -v performance_schema | grep -v information_schema | grep -v mysql > /tmp/bases-mysql
		  BANCOS=`cat /tmp/bases-mysql`
		     for bd in $BANCOS; do
		  	  DATA=`date +%Y-%m-%d`
		        if [ "$DRY" == "1" ]; then
			  echo $DATA-$SRV-mysql-$dbhost-$bd.sql.gz >> $DRY_TEMP
			else
		          mysqldump -u $MYSQL_USER -p"$MYSQL_PASS" -h $dbhost $bd > $TEMP/$SRV-mysql-$dbhost-$bd.sql	
		          gzip $TEMP/$SRV-mysql-$dbhost-$bd.sql
                	  mv $TEMP/$SRV-mysql-$dbhost-$bd.sql.gz $TEMP/$DATA-$SRV-mysql-$dbhost-$bd.sql.gz
			  upload_to_s3 $1 $DATA-$SRV-mysql-$dbhost-$bd.sql.gz       
			fi
		     done
		done
	fi
	
	########
	# BKP DE DIR
	########
	for DIR in $DIR_BACKUP; do
		DIR_NAME=`echo $DIR | sed 's#/#\-#g'`
		DATA=`date +%Y-%m-%d`
                if [ "$DRY" == "1" ]; then
		   echo $DATA-$SRV-dir$DIR_NAME.$TAR_EXT >> $DRY_TEMP
		else
		  tar $TAR_CFG $TEMP/$SRV-dir$DIR_NAME.$TAR_EXT $DIR
                  mv $TEMP/$SRV-dir$DIR_NAME.$TAR_EXT $TEMP/$DATA-$SRV-dir$DIR_NAME.$TAR_EXT
		  upload_to_s3 $1 $DATA-$SRV-dir$DIR_NAME.$TAR_EXT       
		fi
	done

	########
	# BKP RECURSIVO DE DIRS
	########
	if [ $BKP_RECURSIVO -gt 0 ]; then
		for dir in $BKP_RECURSIVO_DIRS; do
                        rm -f /tmp/recursivo-dir 2> /dev/null
			ls $dir > /tmp/recursivo-dirs
			DIR_SITES=`cat /tmp/recursivo-dirs`
			DIR_NAME=`echo $dir | sed 's#/#\-#g'`
			for i in $DIR_SITES; do
			      DATA=`date +%Y-%m-%d`
			      if [ "$DRY" == "1" ]; then
			        echo $DATA-$SRV-dir$DIR_NAME-$i.$TAR_EXT >> $DRY_TEMP	
			      else
				tar $TAR_CFG $TEMP/$SRV-dir$DIR_NAME-$i.$TAR_EXT $dir/$i
                  	      	mv $TEMP/$SRV-dir$DIR_NAME-$i.$TAR_EXT $TEMP/$DATA-$SRV-dir$DIR_NAME-$i.$TAR_EXT
                              	upload_to_s3 $1 $DATA-$SRV-dir$DIR_NAME-$i.$TAR_EXT
			      fi
			done
		done
	fi

	if [ $ERRO -gt 0 ] ; then
		if [ $ENVIA_EMAIL -gt 0 ] ; then
			/usr/bin/sendEmail -u "[$SRV] Problema no backup do servidor" -t $EMAIL_TO -s $EMAIL_SERVER -xu $EMAIL_AUTH_USER -xp $EMAIL_AUTH_PASS -f $EMAIL_FROM -o tls=yes -m "[$SRV] \nSou script /scripts/s3-bkp/s3-bkp-server.sh (bucket s3: $1). \n  \nTentei fazer o backup dos arquivos e subi-los para o S3, mas houveram problemas. \n\nSera preciso rodar o backup manualmente. Para isso voce podera usar o script: \n/scripts/s3-bkp/s3-bkp-server.sh $1 \n \nFavor checar URGENTEMENTE para que o servidor nao fique sem backups."
		fi
	fi

	if [ "$DRY" == "1" ]; then
		echo "INFO: s3-bkp-server.sh executado em modo --dry (apenas teste)..."
		echo "FASE 1: Lista de arquivos de backup (que deveriam estar no S3): "
 		echo "----------------"
		cat $DRY_TEMP | tee $DRY_OUTPUT
 		echo "----------------"
		echo "Resultado em $DRY_OUTPUT"	
 		echo " "
		echo "FASE 2: validando se os arquivos existem no s3..."	
		$AWS_CLI s3 ls s3://$1/ > $TEMP/s3-bucket-output.txt
		for file in `cat $DRY_TEMP`; do
			OUTPUT=`cat $TEMP/s3-bucket-output.txt | grep $file`
			if [ "$OUTPUT" ]; then
				echo "OK   - - $file" >> $DRY_CHECK_TEMP
			else
				echo "ERRO - - $file" >> $DRY_CHECK_TEMP
			fi
		done
		cat $DRY_CHECK_TEMP | tee $DRY_CHECK_OUTPUT
 		echo "----------------"
		echo "Arquivos com OK, estao no S3. Arquivos com ERRO nao estao no S3"
		echo "Resultado em $DRY_CHECK_OUTPUT"	
 		echo "------- FIM ---------"
	fi
	
	rm -rf $TEMP
else
        echo "ERRO: Digite s3-bkp-server.sh nomedobucket"
fi

