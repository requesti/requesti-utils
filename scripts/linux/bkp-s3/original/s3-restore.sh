#!/bin/bash

###
# Script: s3-bkp.sh v.1.6
###

CONFIG=/scripts/s3-bkp/s3-config.sh

if [ -f $CONFIG ]; then
	. $CONFIG
else
    echo "s3-restore.sh: Arquivo de configuracoes $CONFIG nao encontrado! "
	exit 1
fi


function syntax-error
{
	echo "ERRO: execute: s3-restore.sh nome-do-bucket input-file dir-destino" 
	echo " "
	echo "onde: nome-do-bucket : bucket do s3 que estao os arquivos de backup"
	echo "      input-file     : nome do arquivo que contem a lista de arquivos a serem restaurados"
	echo "	              (use o s3-bkp-server.sh com opcao --dry para gerar um inputfile com todos os arquivos de backup)"
	echo "      dir-destino    : nome do diretorio de destino para restauracao (se não existir, será criado)"
} 

if [ "$1" ] && [ "$2" ] && [ "$3" ]; then	
	BUCKET=$1
	INPUT_FILE=$2
	DESTINO=$3
	TEMP_S3=$DESTINO/TEMP_S3

	mkdir -p $DESTINO
	mkdir -p $TEMP_S3
	for file in `cat $INPUT_FILE`; do
	   echo "s3_restore.sh: Iniciando restore do arquivo $file..."
	   $AWS_CLI s3 cp s3://$BUCKET/$file $TEMP_S3
	   CHECK=`echo $file | grep $TAR_EXT`
           if [ "$CHECK" ]; then
	      echo "s3_restore.sh: Percebi que eh um arquivo tar, descompactando-o em $DESTINO..."
	      tar $TAR_EXTRACT $TEMP_S3/$file -C $DESTINO/
	   else
	     CHECK2=`echo $file | grep mysql`
             if [ "$CHECK2" ]; then
	        echo "s3_restore.sh: Percebi que eh um arquivo de dump do mysql, descompactando-o em $DESTINO..."
	        mv $TEMP_S3/$file $DESTINO
  	        gunzip $DESTINO/$file
  	     else
	        echo "s3_restore.sh: Nao consegui entender o formato do arquivo... ele ficara em $TEMP_S3..."
	     fi
           fi	
	done 
	   echo "s3_restore.sh: Saindo..."
	
else
	syntax-error
fi

