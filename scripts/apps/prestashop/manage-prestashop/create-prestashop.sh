#!/bin/bash

#######################################################################
######################### ALTERAR VARIAVEIS ###########################
#######################################################################

### HOST DO BANCO DE DADOS ONDE A BASE DA APLICACAO SERA CRIADA
DB_HOST="yajirobe.servers.requesti.com.br"
### USER DO BANCO DE DADOS ACIMA
ROOT_DB_USER="requesti"
### DIRETORIO ONDE A APLICACAO SERA CRIADA
APP_DIR="/apps"
### USUARIO DO SERVIDOR WEB APACHE OU NGINX
HTTP_USER="nginx:nginx"
### DIRETORIO TEMPORARIO
TEMP_DIR="/tmp/manage-prestashop"
### ARQUIVO DE VHOST EXEMPLO
ARQUIVOVHOSTEXEMPLO="/etc/nginx/vhosts/vhost-nginx"
### DIRETORIO PARA VHOSTS DO WEBSERVER
VHOSTSDIR="/etc/nginx/vhosts"
### SERVICO WEB (nginx ou httpd)
WEBSERVICE="nginx"
### NOME DA APP PARA INSERIR NO ARQUIVO DE SENHAS
APP="PRESTASHOP"
### DIRETORIO DO SCRIPT
SCRIPT_DIR="/scripts/manage-prestashop"
### ARQUIVO QUE CONTEM AS SENHAS CRIADAS
PASSFILE="$SCRIPT_DIR/passwords.txt"
### DIRETORIO QUE CONTEM A APLICACAO DO WORDPRESS
APP_LOCAL="$SCRIPT_DIR/prestashop/"
### URL PARA DOWNLOAD DA ULTIMA VERSAO
LATEST_URL="https://download.prestashop.com/download/releases/prestashop_1.6.1.7_pt.zip"

#######################################################################
########################## INICIO DO SCRIPT ###########################
#######################################################################

rpm -qa | grep '^expect' &> /dev/null
if [ $? -ne 0 ]; then
echo 'Programa "expect" utilizado para gerar caracteres nao esta instalado. Cancelando...'
exit
fi

if [ ! -f "$ARQUIVOVHOSTEXEMPLO" ]; then
echo "Arquivo de vhost exemplo $ARQUIVOVHOSTEXEMPLO nao existe"
exit
fi

if [ ! -d "$SCRIPT_DIR" ]; then
mkdir "$SCRIPT_DIR" -p
fi

if [ ! -d "$TEMP_DIR" ]; then
mkdir "$TEMP_DIR" -p
fi

DB_USER=`mkpasswd -l 8 -d 0 -C 8 -c 0 -s 0`
DB_PASS=`mkpasswd -l 20 -d 6 -C 8 -c 6 -s 0`

while [ $(cat $PASSFILE | grep "$DB_USER") ]; do
echo 'Gerando usuario novamente pois este ja existe' ; DB_USER=`mkpasswd -l 8 -d 0 -C 8 -c 0 -s 0`; done
while [ $(echo $DB_PASS | egrep ':|/|\\') ]; do
echo 'Senha gerada contem caracteres invalidos' ; echo 'gerando novamente...' ; DB_PASS=`mkpasswd -l 20 -d 6 -C 8 -c 6 -s 0`; done

if [ -e "$PASSFILE" ] ; then
clear
echo
echo \***************************************************************
echo
echo RELACAO DE SITES, BANCOS DE DADOS E USUARIOS \******************
cat "$PASSFILE"
echo
echo \***************************************************************
echo
else
clear
echo "Arquivo de senhas nao existe. Criando arquivo "$PASSFILE"..."
echo
echo \***************************************************************
echo 'APP:APP_NAME:DB_NAME:DB_USER:DB_PASS' > $PASSFILE
fi

echo
echo "Digite a senha do banco de dados - $DB_HOST - para iniciar a criacao da base:"
read ROOT_DB_PASS

TESTANDOCONEXAOBD=`mysql -u"$ROOT_DB_USER" -p"$ROOT_DB_PASS" -h "$DB_HOST" -e "show databases;"`
if [ "$?" != "0" ]; then
echo 'ERRO: Erro ao tentar conexao com o Banco de Dados. Cancelando...'
exit
fi

echo
echo "Digite o endereco do novo site: (Ex: www.requesti.com.br) Sera usado na criacao da pasta do site (No diretório $APP_DIR)."
read APP_NAME

if [ -e "$APP_DIR/$APP_NAME" ] ; then

echo 'Nome do site/Diretorio ja existe. Cancelando...'
exit
fi
echo
echo 'Digite o nome que sera usado para o banco de dados: (Ex: requesti ou wwwrequesti)'
read DB_NAME
DB_USER=$DB_NAME$DB_USER

COMPARANDODBNAME=`mysql -u"$ROOT_DB_USER" -p"$ROOT_DB_PASS" -h "$DB_HOST" -e "show databases;" | grep "^$DB_NAME$"`

if [ ! -z $COMPARANDODBNAME ]; then
echo 'Este nome de base ja existe!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Cancelando...'
exit
fi

CRIANDOBANCODEDADOS=`mysql -u"$ROOT_DB_USER" -p"$ROOT_DB_PASS" -h "$DB_HOST" -e "create database $DB_NAME; create user '$DB_USER'@'%' identified by '$DB_PASS'; grant all privileges on $DB_NAME.* to '$DB_USER'@'%' with grant option"`

if [ "$?" != "0" ]; then
echo 'ERRO: Erro ao tentar criar Base de Dados. Cancelando...'
exit
fi

mkdir $APP_DIR/$APP_NAME/public_html -p
if [ ! -d "$APP_LOCAL" ]; then
mkdir "$APP_LOCAL" -p
cd "$TEMP_DIR"
wget "$LATEST_URL"
unzip *.zip
rm -rf *.zip
mv prestashop/* "$APP_LOCAL"
chown -R "$HTTP_USER" "$APP_LOCAL"
rm -rf "$TEMP_DIR"
fi

cd $APP_DIR/$APP_NAME/public_html
rsync -a $APP_LOCAL $APP_DIR/$APP_NAME/public_html/
chown -R "$HTTP_USER" "$APP_DIR"
chmod -R 755 "$APP_DIR"

cp -rapf "$ARQUIVOVHOSTEXEMPLO" "$VHOSTSDIR/$APP_NAME.conf"
sed -i "s/www.nomedosite.com.br/$APP_NAME/g" "$VHOSTSDIR/$APP_NAME.conf"

service $WEBSERVICE reload
if [ "$?" = "0" ]; then
echo
else
echo 'erro no restart do servico web'
fi

echo $APP:$APP_NAME:$DB_NAME:$DB_USER:$DB_PASS >> "$PASSFILE"
