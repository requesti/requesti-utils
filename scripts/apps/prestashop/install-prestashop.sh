#!/bin/bash

TEMP_DIR="/tmp/install-joomla"

LATEST_URL="https://download.prestashop.com/download/releases/prestashop_1.6.1.7_pt.zip"

mkdir $TEMP_DIR -p

echo -ne "Digite o diretorio da aplicacao: (Ex: /var/www/) " ; read DIR
if [ ! -d "$DIR" ]; then
echo "ERRO: O diretorio nao existe! Cancelando..."
exit
fi
cd $TEMP_DIR
wget $LATEST_URL
unzip *.zip
rm -rf *.zip
mv prestashop/* $DIR

rm -rf $TEMP_DIR
