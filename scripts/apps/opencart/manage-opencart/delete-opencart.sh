#!/bin/bash

#######################################################################
######################### ALTERAR VARIAVEIS ###########################
#######################################################################

### HOST DO BANCO DE DADOS ONDE A BASE DA APLICACAO SERA CRIADA
DB_HOST="yajirobe.servers.requesti.com.br"
### USER DO BANCO DE DADOS ACIMA
DB_USER="requesti"
### DIRETORIO ONDE A APLICACAO SERA CRIADA
APP_DIR="/apps"
### USUARIO DO SERVIDOR WEB APACHE OU NGINX
HTTP_USER="nginx:nginx"
### DIRETORIO TEMPORARIO
TEMP_DIR="/tmp/manage-opencart"
### ARQUIVO DE VHOST EXEMPLO
ARQUIVOVHOSTEXEMPLO="/etc/nginx/vhosts/vhost-nginx"
### DIRETORIO PARA VHOSTS DO WEBSERVER
VHOSTSDIR="/etc/nginx/vhosts"
### SERVICO WEB (nginx ou httpd)
WEBSERVICE="nginx"
### NOME DA APP PARA INSERIR NO ARQUIVO DE SENHAS
APP="OPENCART"
### DIRETORIO DO SCRIPT
SCRIPT_DIR="/scripts/manage-opencart"
### ARQUIVO QUE CONTEM AS SENHAS CRIADAS
PASSFILE="$SCRIPT_DIR/passwords.txt"
### DIRETORIO QUE CONTEM A APLICACAO DO WORDPRESS
APP_LOCAL="$SCRIPT_DIR/opencart/"

#######################################################################
########################## INICIO DO SCRIPT ###########################
#######################################################################

clear
echo
echo \***************************************************************
echo
echo RELAÇÃO DE SITES, BANCOS DE DADOS E USUÁRIOS \******************
cat "$PASSFILE"
echo
echo \***************************************************************

echo
echo "Digite a senha do banco de dados - $DB_HOST - para iniciar a remocao da base:"
read DB_PASS

TESTANDOCONEXAOBD=`mysql -u$DB_USER -p$DB_PASS -h $DB_HOST -e "show databases;"`

if [ "$?" = "0" ]; then
echo
else
echo 'ERRO: Erro ao tentar conexao com o Banco de Dados. Cancelando...'
exit
fi

### PERGUNTANDO QUAL APLICACAO SERA REMOVIDA E PESQUISANDO NO ARQUIVO SE TEM OU NAO
### SE OCORRENCIAS FOR MAIOR QUE 1 OU NAO EXISTIR, SE REPETE NO WHILE
WHILE_UNIQ=0
while [ $WHILE_UNIQ = 0 ]; do

echo "Digite o endereco do site que deseja remover, ou entao o usuario deste site no banco de dados: "
read SEARCH_APP

if [ $(cat $PASSFILE | grep $SEARCH_APP | wc -l) -gt 1 ] || [ $(cat $PASSFILE | grep $SEARCH_APP | wc -l) = 0 ]; then
FOUND_LINES=$(cat $PASSFILE | grep $SEARCH_APP | wc -l)
echo "ENCONTRADO(S) $FOUND_LINES OCORRENCIAS EM $PASSFILE. Tente pesquisar novamente."
echo
else
echo "A seguinte aplicacao foi encontrada:"
echo "$(cat $PASSFILE | grep $SEARCH_APP)"
echo -ne "Deseja prosseguir? [y/N]: " ; read CONFIRMA_0
if [ "$CONFIRMA_0" = y ]; then
echo "Ok! Prosseguindo..."
echo
WHILE_UNIQ=1
else
echo "Nao confirmado... Retornando..."
echo
fi
fi
done

LINE_APP=$(cat $PASSFILE | grep $SEARCH_APP)

### DEFININDO QUAL E O NOME DA APLICACAO DE ACORDO COM A PESQUISA
APP_NAME=$(echo $LINE_APP | cut -d":" -f2)
DBNAME=$(echo $LINE_APP | cut -d":" -f3)
USERNAME=$(echo $LINE_APP | cut -d":" -f4)

if [ ! -e "$APP_DIR/$APP_NAME" ]; then
echo 'Nome do site/Diretorio nao existe. Cancelando...'
exit
fi

COMPARANDODBNAME=`mysql -u$DB_USER -p$DB_PASS -h $DB_HOST -e "show databases;" | grep "^$DBNAME$"`

if [ -z $COMPARANDODBNAME ]; then
echo 'Este nome de base NÃO existe!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!. Cancelando...'
exit
else
        echo
        echo "Nome existe. Continuando..."
        echo
fi

COMPARANDOUSERNAME=`mysql -u$DB_USER -p$DB_PASS -h $DB_HOST -e "SELECT User FROM mysql.user;" | grep "^$USERNAME$"`

if [ -z $COMPARANDOUSERNAME ]; then
echo 'Este nome de user NÃO existe!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!. Cancelando...'
exit
else
        echo
        echo "Nome de user existe. Continuando..."
        echo
fi

REMOVENDOBANCODEDADOS=`mysql -u$DB_USER -p$DB_PASS -h $DB_HOST -e "drop database $DBNAME; drop user $USERNAME;"`

rm -rf $APP_DIR/$APP_NAME
rm -rf $VHOSTSDIR/$APP_NAME.conf

service $WEBSERVICE reload
if [ "$?" = "0" ]; then
echo
else
echo 'erro no reload do servico web'
fi

sed -i "/^$APP:$APP_NAME:/d" $PASSFILE
