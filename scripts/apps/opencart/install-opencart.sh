#!/bin/bash

TEMP_DIR="/tmp/install-opencart"

LATEST_URL="https://github.com/opencart/opencart/releases/download/2.3.0.2/2.3.0.2-compiled.zip"

mkdir $TEMP_DIR -p

echo -ne "Digite o diretorio da aplicacao: (Ex: /var/www/) " ; read DIR
if [ ! -d "$DIR" ]; then
echo "ERRO: O diretorio nao existe! Cancelando..."
exit
fi
cd $TEMP_DIR
wget $LATEST_URL
unzip *.zip
rm -rf *.zip
mv upload/config-dist.php upload/config.php
mv upload/admin/config-dist.php upload/admin/config.php
mv upload/* $DIR

rm -rf $TEMP_DIR
