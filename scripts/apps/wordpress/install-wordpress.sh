#!/bin/bash

TEMP_DIR="/tmp/install-wordpress"

mkdir $TEMP_DIR -p

echo -ne "Digite o diretorio da aplicacao: (Ex: /var/www/) " ; read DIR
if [ ! -d "$DIR" ]; then
echo "ERRO: O diretorio nao existe! Cancelando..."
exit
fi
cd $TEMP_DIR
wget https://wordpress.org/latest.tar.gz
tar zxf latest.tar.gz
mv wordpress/* $DIR

rm -rf $TEMP_DIR
