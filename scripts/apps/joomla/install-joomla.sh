#!/bin/bash

TEMP_DIR="/tmp/install-joomla"

LATEST_URL="https://github.com/joomla/joomla-cms/releases/download/3.6.2/Joomla_3.6.2-Stable-Full_Package.zip"

mkdir $TEMP_DIR -p

echo -ne "Digite o diretorio da aplicacao: (Ex: /var/www/) " ; read DIR

if [ ! -d "$DIR" ]; then
	echo "ERRO: O diretorio nao existe! Cancelando..."
	exit
fi

cd $TEMP_DIR
wget $LATEST_URL
unzip *.zip
rm -rf *.zip
mv * $DIR
rm -rf $TEMP_DIR
