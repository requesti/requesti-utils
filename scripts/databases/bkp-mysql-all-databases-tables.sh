#!/bin/bash

#######################################################################
######################### ALTERAR VARIAVEIS ###########################
#######################################################################

### INSIRA O NOME DE USUARIO DO BANCO DE DADOS
MYSQL_USER='bkpmysql'
### INSIRA A SENHA DO USUARIO DO BANCO DE DADOS
MYSQL_PASS='H58&^4$3Pr'
### ENDEREÇO DO SERVIDOR BANCO DE DADOS REMOTO (127.0.0.1 CASO BANCO DE DADOS FOR LOCAL)
MYSQL_HOST="localhost"
### PORTA PARA CONEXAO AO BANCO DE DADOS (PADRAO 3306)
MYSQL_PORT="3306"
### BASES DE DADOS QUE O SCRIPT IRA IGNORAR (MÁXIMO DE 20 BASES)
MYSQL_IGNORE="information_schema innodb mysql performance_schema tmp"
### ENVIA EMAIL CASO FALHE? (1=sim, 0=nao)
ENVIA_EMAIL=1
### DIRETORIO DESTINO DO ARQUIVO DE BACKUP
BKP_DIR="/tmp/bkp_db2"
### DIRETORIO PARA CRIACAO DE ARQUIVOS TEMPORARIOS
TEMP_DIR="/tmp/bkp_db"
### ARQUIVO ONDE SERA GRAVADO OS LOGS E TODAS A SAIDAS
LOGFILE="/var/log/bkp-all-databases.log"
### TAMANHO MAXIMO DO ARQUIVO DE LOG (EM BYTES)
MAX_LOG="104857600" ### 100Mb

# Onde esta o cli da AWS
# CentOS
AWS_CLI=/usr/bin/aws
# Ubuntu
#AWS_CLI=/usr/local/bin/aws

#######################################################################
############### CONFIGURACOES PARA ENVIO DE E-MAIL ####################
#######################################################################

##EMAIL_TO="backupmon@brlink.com.br"
##EMAIL_FROM="scripts.servers@clientes.brlink.com.br"
##EMAIL_AUTH_USER="scripts.servers@clientes.brlink.com.br"
##EMAIL_AUTH_PASS="@p3SoL0R"
##EMAIL_SERVER="smtpserver.brlink.com.br:587"
###USE_TLS="yes" ### yes OR no OR auto

#######################################################################
#ENVIA_EMAIL=1

EMAIL_TO="willian@ecletica.com.br;rafael@ecletica.com.br;ecletica@ecletica.com.br"
EMAIL_FROM="pedidosweb@ecletica.com.br"
EMAIL_SERVER="smtp.ecletica.com.br:587"
#### INSIRA AS CREDENCIAIS
EMAIL_AUTH_USER="pedidosweb@ecletica"
EMAIL_AUTH_PASS="aerdna"

#######################################################################
########################## INICIO DO SCRIPT ###########################
#######################################################################

### VERIFICANDO BUCKET
if [ -z $1 ]; then
echo 'ERRO: Digite o nome do bucket como parametro. Exemplo:'
echo "sh $0 nomedobucket"
fi

### ROTACIONANDO LOGS
if [ -e $LOGFILE ]; then
if [ "$(ls -la "$LOGFILE" | cut -d" " -f5)" -ge "$MAX_LOG" ]; then
tail -100000 $LOGFILE > "$LOGFILE"2
cat "$LOGFILE"2 > $LOGFILE
rm -rf "$LOGFILE"2
fi
fi

### REDIRECIONANDO SAIDAS PARA O ARQUIVO DE LOG
exec &>> $LOGFILE

echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Inicio do Script --"
COUNT_VAR=$(echo $MYSQL_IGNORE | wc -w)
if [ "$(echo $COUNT_VAR)" -gt 20 ]; then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Quantidade de bases a desconsiderar e maior que 20. Script nao funciona bem com esta quantidade, portanto e necessario efetuar alteracoes na quantidade ou no codigo do script --"
exit 1
fi

/usr/bin/mysql -u $MYSQL_USER --password=$MYSQL_PASS -h $MYSQL_HOST -P $MYSQL_PORT -e 'show databases' -s --skip-column-names > /dev/null 2>> $LOGFILE

if [ $? -ne 0 ]; then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Erro ao conectar no banco de dados informado. Verificar --"
if [ "$ENVIA_EMAIL" = 1 ]; then
/usr/bin/sendEmail -u "[$(hostname)] Script: $0" -t $EMAIL_TO -s $EMAIL_SERVER -xu $EMAIL_AUTH_USER -xp $EMAIL_AUTH_PASS -f $EMAIL_FROM -m "[$(hostname)] \nSou script $0, executado no servidor $(hostname). Desta vez, Tive um problema de execucao. Por favor, verificar nos logs o que pode ter acontecido. \n\nPrevia do log: \n $(tail -20 $LOGFILE)"
fi
echo "##########################################################"
exit 1
fi

if [ ! -d "$BKP_DIR" ]; then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Criando diretorio de backup "$BKP_DIR", pois nao existe --"
mkdir -p "$BKP_DIR"
fi

if [ ! -d "$TEMP_DIR" ]; then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Criando diretorio temporario "$TEMP_DIR", pois nao existe --"
mkdir -p "$TEMP_DIR"
fi

echo "SET autocommit=0;SET unique_checks=0;SET foreign_key_checks=0;" > $TEMP_DIR/bkp_bd_tmp_sqlhead.sql
echo "SET autocommit=1;SET unique_checks=1;SET foreign_key_checks=1;" > $TEMP_DIR/bkp_bd_tmp_sqlend.sql

for I in $(/usr/bin/mysql -u $MYSQL_USER --password=$MYSQL_PASS -h $MYSQL_HOST -P $MYSQL_PORT -e 'show databases' -s --skip-column-names);
do
if [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f1)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f2)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f3)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f4)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f4)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f5)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f6)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f7)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f8)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f9)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f10)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f11)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f12)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f13)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f14)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f15)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f16)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f17)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f18)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f19)" ] || [ "$I" = "$(echo $MYSQL_IGNORE | cut -d" " -f20)" ]
then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Desconsiderando tabelas da base $I --"
continue
fi
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Exportando tabelas da base $I --"
for J in $(/usr/bin/mysql -u $MYSQL_USER --password=$MYSQL_PASS -h $MYSQL_HOST -P $MYSQL_PORT $I -e "show tables;" | grep -v Tables_in_);
do
/usr/bin/mysqldump -u $MYSQL_USER --password=$MYSQL_PASS -h $MYSQL_HOST -P $MYSQL_PORT $I $J | cat $TEMP_DIR/bkp_bd_tmp_sqlhead.sql - $TEMP_DIR/bkp_bd_tmp_sqlend.sql | gzip -fc > "$BKP_DIR/$(date +%Y%m%d)-$I-$J.sql.gz"
done
done

$AWS_CLI s3 cp $BKP_DIR s3://$1/ --recursive

if [ $? -ne 0 ]; then
echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Erro ao subir arquivos no s3. Verificar --"
if [ "$ENVIA_EMAIL" = 1 ]; then
/usr/bin/sendEmail -u "[$(hostname)] Script: $0" -t $EMAIL_TO -s $EMAIL_SERVER -xu $EMAIL_AUTH_USER -xp $EMAIL_AUTH_PASS -f $EMAIL_FROM -m "[$(hostname)] \nSou script $0, executado no servidor $(hostname). Desta vez, Tive um problema de execucao. Por favor, verificar nos logs o que pode ter acontecido. \n\nPrevia do log: \n $(tail -20 $LOGFILE)"
fi
echo "##########################################################"
exit 1
fi

rm -rf $TEMP_DIR/bkp_bd_*
rm -rf $BKP_DIR

echo "$(date +%d/%m/%Y) $(date +%H:%M:%S:%N) -- Fim do script --"
echo "##########################################################"
