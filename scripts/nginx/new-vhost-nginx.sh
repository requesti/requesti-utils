#!/bin/bash

APPS_DIR="/apps"

echo "Digite o endereco do novo site: (Ex: www.requesti.com.br) Sera usado na criacao da pasta do site. (CTRL C para cancelar)"
read SITENAME

mkdir $APPS_DIR/$SITENAME/public_html -p

cp /etc/nginx/vhosts/vhost-nginx /etc/nginx/vhosts/$SITENAME.conf
sed -i "s/www.nomedosite.com.br/$SITENAME/g" /etc/nginx/vhosts/$SITENAME.conf

touch $APPS_DIR/$SITENAME/public_html/index.html
echo "$SITENAME" >> $APPS_DIR/$SITENAME/public_html/index.html

SITENAME=$(echo $SITENAME | cut -d'.' -f2,3,4,5,6,7,8,9)
sed -i "s/nomedosite.com.br/$SITENAME/g" /etc/nginx/vhosts/*$SITENAME.conf

chown -R nginx:nginx $APPS_DIR
chmod -R 755 $APPS_DIR

service nginx reload
######
######