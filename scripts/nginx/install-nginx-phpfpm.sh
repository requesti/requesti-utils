#!/bin/bash

### INSTALAR
yum install nginx php-devel php-fpm php-mysql php-mcrypt php-gd php-mbstring php-dom -y
mkdir /etc/nginx/vhosts -p

### ALTERAR USER CONF NGINX
sed -i 's/user = apache/user = nginx/g' /etc/php-fpm.d/www.conf
sed -i 's/group = apache/group = nginx/g' /etc/php-fpm.d/www.conf

### ALTERANDO CONFS DO NGINX
#sed -i 's/conf.d/vhosts/g' /etc/nginx/nginx.conf
#sed -i 's/worker_processes auto/worker_processes 5/g' /etc/nginx/nginx.conf
#sed -i 's/worker_connections 1024/worker_connections 2048/g' /etc/nginx/nginx.conf
#sed -i 's/keepalive_timeout   65/keepalive_timeout   5/g' /etc/nginx/nginx.conf
#sed -i 's/80 default_server;/80;/g' /etc/nginx/nginx.conf
#sed -i '18i\map $scheme $fastcgi_https { ## Detect when HTTPS is used\' /etc/nginx/nginx.conf
#sed -i '19i\default off;\' /etc/nginx/nginx.conf
#sed -i '20i\https on;\' /etc/nginx/nginx.conf
#sed -i '21i\}\' /etc/nginx/nginx.conf
#sed -i '22i\rewrite_log on;\' /etc/nginx/nginx.conf
#sed -i '23i\gzip on;\' /etc/nginx/nginx.conf
#sed -i '24i\ gzip_disable “msie6”;\' /etc/nginx/nginx.conf
#sed -i '25i\ gzip_vary on;\' /etc/nginx/nginx.conf
#sed -i '26i\ gzip_proxied any;\' /etc/nginx/nginx.conf
#sed -i '27i\ gzip_comp_level 5;\' /etc/nginx/nginx.conf
#sed -i '28i\ gzip_buffers 16 8k;\' /etc/nginx/nginx.conf
#sed -i '29i\ gzip_http_version 1.1;\' /etc/nginx/nginx.conf
#sed -i '30i\ gzip_types text/plain text/css application/json application/x-javascript\' /etc/nginx/nginx.conf
#sed -i '31i\text/xml application/xml application/xml+rss text/javascript;\' /etc/nginx/nginx.conf
#sed -i '32i\client_max_body_size 100m;\' /etc/nginx/nginx.conf
#sed -i '33i\server_tokens off;\' /etc/nginx/nginx.conf
rm -rf /etc/nginx/nginx.conf
cd /etc/nginx/ ; wget https://bitbucket.org/requesti/requesti-utils/raw/master/scripts/nginx/nginx.conf

### BAIXANDO VHOST EXEMPLO
cd /downloads
wget https://bitbucket.org/requesti/requesti-utils/raw/master/scripts/nginx/vhost-nginx
mv vhost-nginx /etc/nginx/vhosts/

### COLOCANDO PROGRAMAS NA INICIALIZAÇÃO DO SERVIDOR
systemctl enable nginx
systemctl enable php-fpm

### CRIANDO PASTA PARA GRAVACAO DE SESSAO (RESOLVENDO UM PROBLEMA COM JOOMLA)
mkdir /var/lib/php/session
chmod 775 /var/lib/php/session/
chown nginx.nginx /var/lib/php/session/

### ATIVAR SERVIÇOS
service php-fpm start
service nginx start

### BAIXANDO E EXECUTANDO SCRIPT PARA CRIAR NOVO VHOST
cd /scripts
wget https://bitbucket.org/requesti/requesti-utils/raw/master/scripts/nginx/new-vhost-nginx.sh
chmod +x new-vhost-nginx.sh

echo -ne "Deseja criar um novo vhost agora? [y/N]: " ; read CONFIRMA_NOVOVHOST
if [ "$CONFIRMA_NOVOVHOST" = y ]; then
sh new-vhost-nginx.sh
fi
