#!/bin/bash
ACCOUNT=$(/usr/bin/aws sts get-caller-identity --output text --query 'Account')
VERBOSE=1 ### Alterar para 1 para detalhar log de erro. Padrao é 0
LOG_DIR=/var/log
LOG_FILE=$LOG_DIR/aws-testsnapshot.log
EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
EC2_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"
NOMECLIENTE=$(cat /tmp/clientname.txt)
RESULT="SNAPSHOT(S) OK"

if [ ! -z $1 ]; then VERBOSE=1; LOG_FILE=/dev/tty; fi
if [ $VERBOSE = 1 ]; then ECHO=log_all; else ECHO=log_error; fi

function log_all {
echo -e "[$(date +%d/%m/%Y) $(date +%H:%M:%S:%N)] [$NOMECLIENTE $ACCOUNT ${EC2_REGION}]: ${*}" &>> ${LOG_FILE}
}

function log_error {
echo -e "${*}" &>> /dev/null
}

function detach_volume_error {
/usr/bin/aws ec2 detach-volume --volume-id $new_volume --force --region $EC2_REGION --output json &> /dev/null; sleep 20; /usr/bin/aws ec2 delete-volume --volume-id $new_volume --region $EC2_REGION --output json &> /dev/null ; $ECHO "Volume removido."
}

function if_error {
if [ $? != 0 ]; then log_all "ERRO: Erro ao executar $1:\n $2"; detach_volume_error; $ECHO "Resultado final: ERRO FATAL" ; exit 1; fi
}

function if_error_ls {
if [ $? != 0 ]; then log_all "ERRO: Integridade do snapshot $1 comprometida!!! Por algum motivo, nao foi possivel listar ou montar o conteudo:\n $2"; RESULT="PROBLEMA ENCONTRADO" ; fi
}

log_all "Iniciando script"
INSTANCE_ID=$(curl -s -k http://169.254.169.254/latest/meta-data/instance-id 2>&1) ; if_error curl-instanceid "$INSTANCE_ID"
$ECHO "ID da instancia criada: \"$INSTANCE_ID\" ..."
VOLUMES=$(/usr/bin/aws ec2 describe-volumes --region $EC2_REGION --filters Name=status,Values=available,in-use --output json 2>&1) ; if_error describe-volumes "$VOLUMES"
ID_VOLS=$(echo "$VOLUMES" | jq -r '.Volumes[].VolumeId' 2>&1) ; if_error id_vols "$ID_VOLS"
$ECHO "Comando describe-volumes executado com sucesso."
SNAPSHOTS=$(/usr/bin/aws ec2 describe-snapshots --region $EC2_REGION --owner self --filter "Name=status,Values=completed" --output json 2>&1) ; if_error describe-snapshots "$SNAPSHOTS"
$ECHO "Comando describe-snapshots executado com sucesso."

for vol in $ID_VOLS; do
        TAG_VOL=$(echo "$VOLUMES" | jq --arg vol "$vol" -r '.Volumes[]|select(.VolumeId|contains($vol))' | jq -r '(.Tags[]|select(.["Key"] == "snapshot")|.Value)' 2>&1 | sed 's/ //g')
    if [ "$TAG_VOL" = 1 ]; then
        VOLS_TAG_1+="$vol "
    fi
done

if [ -z "$VOLS_TAG_1" ]; then
        $ECHO "NENHUM VOLUME COM A TAG 1 ENCONTRADO!"
        exit
fi

for vol in $VOLS_TAG_1; do
        TAG_BKPSNAP=$(echo "$VOLUMES" | jq --arg vol "$vol" -r '.Volumes[]|select(.VolumeId|contains($vol))' | jq -r '(.Tags[]|select(.["Key"] == "bkpsnap")|.Value)' 2>&1 | sed 's/ //g')
    if [ "$TAG_BKPSNAP" = 0 ]; then
        VOLS_TAG_1=$(echo "$VOLS_TAG_1" | sed "s/$vol//g")
        VOLS_TAG_BKPSNAP+="$vol "
    fi
done
if [ ! -z "$VOLS_TAG_BKPSNAP" ]; then $ECHO "ATENCAO: Volumes com tag bkpsnap=0 (serao ignorados): $VOLS_TAG_BKPSNAP"; fi

for vol in $VOLS_TAG_1; do
    productcode=$(/usr/bin/aws ec2 describe-volume-attribute --region $EC2_REGION --volume-id $vol --attribute productCodes --output json 2>&1 | grep marketplace)
    if [ ! -z "$productcode" ]; then
        VOLS_PRODUCT_CODE+="$vol "
        VOLS_TAG_1=$(echo "$VOLS_TAG_1" | sed "s/$vol//g")
    fi
done
if [ ! -z "VOLS_PRODUCT_CODE" ]; then $ECHO "ATENCAO: Volumes travados com productcode (serao ignorados): $VOLS_PRODUCT_CODE"; fi

$ECHO "Volume(s) com tag snapshot=1 encontrado(s): $VOLS_TAG_1"

for vol in $VOLS_TAG_1; do
        LASTSNAP=$(echo "$SNAPSHOTS" | jq --arg vol "$vol" -r '[.Snapshots[]|select(.VolumeId == $vol)] | max_by(.StartTime) | .SnapshotId')
        if [ $LASTSNAP != null ]; then
                LAST_SNAPS+="$LASTSNAP "
        else
                SNAP_NULL+="$vol "
        fi
done

$ECHO "Snapshots destes volumes: $LAST_SNAPS"
if [ ! -z "$SNAP_NULL" ]; then
$ECHO "Os seguintes volumes estao com a tag setada porem nao tem snapshots: $SNAP_NULL"
fi
if [ -z "$LAST_SNAPS" ]; then
        $ECHO "Nenhum snapshot para ser validado!!!"
        exit
fi

COUNT1=0
/usr/bin/aws ec2 describe-snapshots --region $EC2_REGION --owner self --output json | grep pending &> /dev/null 2>&1
        while [ $? = 0 ]; do
                $ECHO "Existe um ou mais snapshots em andamento. Verificando novamente em 60 segundos."
                if [ $COUNT1 -ge 10 ]; then log_all TIMEOUT 10M: snapshot em andamento. Cancelando execucao.; exit 1; fi
                COUNT1=$(expr $COUNT1 + 1)
                sleep 60
                /usr/bin/aws ec2 describe-snapshots --region $EC2_REGION --owner self --output json | grep pending &> /dev/null 2>&1
        done

for snap in $LAST_SNAPS; do
        COUNT2=0
        $ECHO "### TESTANDO snapshot: $snap"
        new_volume=$(/usr/bin/aws ec2 create-volume --region $EC2_REGION --availability-zone $EC2_AVAIL_ZONE --snapshot-id $snap --volume-type gp2 --output json | jq -r '.VolumeId' 2>&1) ; if_error create-volume "$new_volume"
        $ECHO "Criacao do volume em andamento. Aguardando termino." ; sleep 10
        /usr/bin/aws ec2 describe-volumes --filters Name=volume-id,Values=$new_volume --region $EC2_REGION --output json| grep available &> /dev/null 2>&1
        while [ $? != 0 ]; do
                $ECHO "Criacao do volume ainda em andamento. Verificando novamente em 10 segundos."
                if [ $COUNT2 -ge 30 ]; then log_all TIMEOUT 5M: criacao de volume nao concluida. Cancelando execucao.; exit 1; fi
                COUNT2=$(expr $COUNT2 + 1)
                sleep 10
                /usr/bin/aws ec2 describe-volumes --filters Name=volume-id,Values=$new_volume --region $EC2_REGION --output json| grep available &> /dev/null 2>&1
        done
        $ECHO "Volume criado corretamente."

        COUNT3=0
        attach=$(/usr/bin/aws ec2 attach-volume --volume-id $new_volume --instance-id $INSTANCE_ID --device /dev/sdk --region $EC2_REGION --output json 2>&1) ; if_error attach-volume-$new_volume "$attach"
    $ECHO "Volume sendo anexado a instancia. Aguardando termino." ; sleep 5
        /usr/bin/aws ec2 describe-volumes --filters Name=volume-id,Values=$new_volume --region $EC2_REGION --output json | grep attached &> /dev/null 2>&1
        while [ $? != 0 ]; do
                $ECHO "Volume ainda sendo anexado. Aguardando termino."
        if [ $COUNT3 -ge 60 ]; then log_all TIMEOUT 5M: falha ao anexar volume a instancia. Cancelando execucao.; detach_volume_error ; exit 1; fi
                COUNT3=$(expr $COUNT3 + 1)
                sleep 5
                /usr/bin/aws ec2 describe-volumes --filters Name=volume-id,Values=$new_volume --region $EC2_REGION --output json | grep attached &> /dev/null 2>&1
        done
        $ECHO "Volume anexado corretamente."

        BLKID=$(blkid | grep xvdk | grep TYPE)
        if [ -z "$BLKID" ]; then
                $ECHO "Nenhuma particao encontrada neste volume!" ; nenhuma-particao-encontrada; if_error_ls $snap "Nenhuma particao foi encontrada!"
        else
        DEV=$(echo "$BLKID" | awk -F":" '{print $1}')
        FS=$(echo "$BLKID" | awk '{for(i=1;i<=NF;i++){if($i~/\<TYPE\>/){print $i}}}' | awk -F"=" '{print $NF}' | sed 's/\"//g')
        echo "$DEV" > /tmp/dev.tmp
        echo "$FS" > /tmp/fs.tmp
        DEV_FS=$(paste /tmp/dev.tmp /tmp/fs.tmp -d :)

                for dev in $DEV_FS; do
            $ECHO "Testando particao: $dev"
                        mkdir /mnt$(echo "$dev" | cut -d":" -f1) -p
            $ECHO "Montando..."
                        mount=$(mount -t $(echo "$dev" | cut -d":" -f2) $(echo "$dev" | cut -d":" -f1) /mnt$(echo "$dev" | cut -d":" -f1) 2>&1) ; if_error_ls $snap "$mount"
            $ECHO "Listando conteudo..."
            sleep 5 ; ls=$(/bin/ls /mnt$(echo "$dev" | cut -d":" -f1)/ 2>&1) ; if_error_ls $snap "$ls"
            if [ -z "$ls" ]; then volume-esta-vazio; if_error_ls $snap "$ls"; fi
            $ECHO $ls
            $ECHO "Desmontando..."
            sleep 2 ; umount=$(umount /mnt$(echo "$dev" | cut -d":" -f1) 2>&1) ; if_error_ls $snap "$umount"
            sleep 2 ; rm=$(rm -rf /mnt$(echo "$dev" | cut -d":" -f1) 2>&1) ; if_error rm-$dev "$umount"
                done
        
        fi
    $ECHO "Teste do snapshot concluido. Desanexando volume."
    COUNT4=0
    detach=$(/usr/bin/aws ec2 detach-volume --volume-id $new_volume --force --region $EC2_REGION --output json 2>&1) ; if_error detach-volume-$new_volume "$detach"
    sleep 5
    /usr/bin/aws ec2 describe-volumes --filters Name=volume-id,Values=$new_volume --region $EC2_REGION --output json | grep available &> /dev/null 2>&1
    while [ $? != 0 ]; do
        $ECHO "Volume ainda nao desanexado. Tentando novamente."
        if [ $COUNT4 -ge 60 ]; then log_all TIMEOUT 5M: falha ao desanexar volume da instancia. Cancelando execucao.; detach_volume_error ; exit 1; fi
        COUNT4=$(expr $COUNT4 + 1)
        sleep 5
        /usr/bin/aws ec2 describe-volumes --filters Name=volume-id,Values=$new_volume --region $EC2_REGION --output json | grep available &> /dev/null 2>&1
    done
    $ECHO "Volume desanexado corretamente."

    delete_volume=$(/usr/bin/aws ec2 delete-volume --volume-id $new_volume --region $EC2_REGION --output json 2>&1) ; if_error delete-volume-$new_volume "$delete_volume"
    $ECHO "Volume $new_volume removido."
done
rm -rf /tmp/dev.tmp /tmp/fs.tmp
$ECHO "Fim do script. Resultado final: $RESULT"
