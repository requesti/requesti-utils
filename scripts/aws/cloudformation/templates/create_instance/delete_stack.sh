#!/bin/bash

EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
EC2_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"

/usr/bin/aws cloudformation describe-stacks --stack-name BRLink-TestSnapshots --region $EC2_REGION --output json | grep CREATE_IN_PROGRESS &> /dev/null

if [ $? != 0 ]; then
	echo "Stack não está mais como CREATE_IN_PROGRESS. Remocão do stack será iniciada em 60 segundos." &>> /var/log/delete_stack.log
	sleep 60
	/usr/bin/aws cloudformation delete-stack --stack-name BRLink-TestSnapshots --region $EC2_REGION --output json
else
	echo "Stack ainda como CREATE_IN_PROGRESS. Consulta novamente em 60 segundos." &>> /var/log/delete_stack.log
fi
