import boto3

region = 'us-east-1 ou sa-east-1'

stackname = 'BRLink-TestSnapshots'
client = boto3.client('cloudformation', region_name=region)
response = client.create_stack(
    StackName= (stackname),
    TemplateURL='https://s3.amazonaws.com/cf-testsnapshot/test_snapshots.json',
    Parameters=[
        {
            'ParameterKey': 'clientname',
            'ParameterValue': 'nome-do-cliente-aqui'
        }],
    Capabilities=[
        'CAPABILITY_IAM',
        ]
)

def lambda_handler(event, context):
    return(response)
