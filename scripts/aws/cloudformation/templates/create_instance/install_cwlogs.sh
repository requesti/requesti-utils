#!/bin/bash

echo -e "[$(date +%d/%m/%Y) $(date +%H:%M:%S:%N)]: Iniciando script" &> /var/log/install_cwlogs.log

rm -rf /etc/localtime
ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

wget https://s3.amazonaws.com/cf-testsnapshot/awslogs-agent-setup.py -P /tmp
mkdir -p /var/awslogs/etc
wget https://s3.amazonaws.com/cf-testsnapshot/aws.conf -P /var/awslogs/etc
wget https://s3.amazonaws.com/cf-testsnapshot/cw-logs.conf -P /etc
wget https://s3.amazonaws.com/cf-testsnapshot/delete_stack.sh -P /tmp
chmod 700 /tmp/awslogs-agent-setup.py /tmp/delete_stack.sh

### Instalando CloudWatch Logs
cd /tmp
python ./awslogs-agent-setup.py --region us-east-1 -c /etc/cw-logs.conf -n

echo -e "[$(date +%d/%m/%Y) $(date +%H:%M:%S:%N)]: Fim do script" &>> /var/log/install_cwlogs.log

echo '* * * * * root sh /tmp/delete_stack.sh' >> /etc/crontab
