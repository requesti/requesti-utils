#!/bin/bash
# Parametros necessarios: # $1 - credenciais do cliente # $2 - regiao a ser checada # $3 opcional = se usado, mostra na tela ao inves do log

VERBOSE=0 ### Alterar para 1 para detalhar log de erro. Padrao é 0
export AWS_CONFIG_FILE=/git/requesti-utils/scripts/aws/checa-snapshots/aws-cred/aws-cred.conf
AWS=/usr/bin/aws
LOG_DIR=/var/log/zabbix/aws-volumes
LOG_FILE=$LOG_DIR/aws-volumes.log
USER=root ### Usuario que o script deve permitir execucao
REGIONS='us-east-1 us-east-2 us-west-1 us-west-2 eu-west-1 eu-central-1 ap-northeast-1 ap-northeast-2 ap-southeast-1 ap-southeast-2 ap-south-1 sa-east-1'
RET=0
RESULT=0
P1=$1
P2=$2

if [ ! -z $3 ]; then VERBOSE=1; LOG_FILE=/dev/tty; fi
if [ $VERBOSE = 1 ]; then ECHO=log_all; else ECHO=log_error; fi

function log_all {
echo -e "[$(date +%d/%m/%Y) $(date +%H:%M:%S:%N)] [${P1} ${P2}] - ${*}" &>> ${LOG_FILE}
}

function log_error {
echo -e "${*}" &>> /dev/null
}

function if_error {
if [ $? != 0 ]; then log_all "ERRO: Erro ao executar $1:\n $2"; echo 1; exit 1; fi
}

if [ ! -w $LOG_DIR ]; then echo -e "ERRO: Diretorio $LOG_DIR nao encontrado. Verifique permissoes ou se diretorio existe."; exit 1; fi
if [ $(whoami) != $USER ]; then echo -e "ERRO: Este script deve ser executado apenas com o usuario $USER:"; echo "sudo -Hu $USER sh $0 $1 $2 $3"; exit 1; fi
if [ -z $1 ] || [ -z $2 ]; then echo -e 'ERRO: Parametros $1 e/ou $2 em branco.'; exit 1; fi
REGIONS="$REGIONS," ; if [ -z $(echo $REGIONS | sed 's/ /,/g' | grep $2,) ]; then echo -e "ERRO: A regiao $2 nao foi encontrada."; exit 1; fi
if ! grep -qs "\[profile $1\]" $AWS_CONFIG_FILE; then echo -e "ERRO: Usuario $1 nao encontrado no arquivo de credenciais. Aguarde ou verifique sync com Socrates."; exit 1; fi
if [ $(date +%A) = Sunday ]; then RET=1; fi

DATE=$(date +%Y-%m-%d -d "$RET day ago")

log_all "Iniciando script"
$ECHO "Data de execucao: $(date +%A). Data de snapshot considerada correta sera: $DATE."

### PRIMEIRA FASE: VERIFICANDO VOLUMES COM SNAPSHOTS E DATA DOS SNAPSHOTS
$ECHO "Executando comando describe-volumes..."
VOLUMES=$($AWS ec2 describe-volumes --profile $1 --region $2 --filters Name=status,Values=available,in-use --output json 2>&1) ; if_error describe-volumes "$VOLUMES"
ID_VOLS=$(echo "$VOLUMES" | jq -r '.Volumes[].VolumeId' 2>&1) ; if_error var-id-vols "$ID_VOLS"
        if [ -z "$ID_VOLS" ]; then
                log_all "ERRO: Nao foi encontrado nenhum volume na regiao informada. O script retornara erro."
                log_all "Fim do script - Resultado final: 1"; echo 1
                exit 1
        else
                $ECHO "Foram encontrados $(echo $ID_VOLS | wc -w) volumes."
        fi
COUNT_ID_VOL=$(echo "$ID_VOLS" | head -n 1 | wc -c) ### Define tamanho do id do volume
        if [ $COUNT_ID_VOL -ge 15 ]; then DESCIDVOL=IDENTIFICACAO_VOL; else DESCIDVOL=ID_DO_VOLUME; fi

ID_VOLS_COMMA=$(echo $ID_VOLS | sed 's/ /,/g' 2>&1) ; if_error var-id-vols-comma "$ID_VOLS_COMMA"
$ECHO "Comando describe-volumes executado com sucesso."
$ECHO "Executando comando describe-snapshots..."
#SNAPSHOTS=$($AWS ec2 describe-snapshots --profile $1 --region $2 --owner self --filter "Name=status,Values=completed" "Name=volume-id,Values=$ID_VOLS_COMMA" --output json 2>&1) ; if_error describe-snapshots "$SNAPSHOTS"
SNAPSHOTS=$($AWS ec2 describe-snapshots --profile $1 --region $2 --owner self --filter "Name=status,Values=completed" --output json 2>&1) ; if_error describe-snapshots "$SNAPSHOTS"
$ECHO "Comando describe-snapshots executado com sucesso."
$ECHO "Descrevendo volumes:"

if [ $VERBOSE = 1 ]; then log_all "$DESCIDVOL\tULTIMOSNAP\tSTATUS"; fi

for vol in $ID_VOLS; do
LASTSNAP=$(echo "$SNAPSHOTS" | jq --arg vol "$vol" -r '[.Snapshots[]|select(.VolumeId == $vol)] | max_by(.StartTime) | .StartTime' 2>&1) ; if_error var-lastsnap "$LASTSNAP"
TAG_VOL=$(echo "$VOLUMES" | jq --arg vol "$vol" -r '.Volumes[]|select(.VolumeId|contains($vol))' | jq -r '(.Tags[]|select(.["Key"] == "snapshot")|.Value)' 2>&1)
if [ $LASTSNAP != null ] && [ "$TAG_VOL" != 0 ]; then
        DATESNAP=$(echo $LASTSNAP | awk -F"T" '{print $1}')
        if [ $DATE = $DATESNAP ]; then
                $ECHO "$vol\t$DATESNAP\tOK (Snapshot OK)"
        else
                RESULT=1
                log_all "$vol\t$DATESNAP\tERRO: Snapshot antigo. Se nao for necessario, colocar tag snapshot=0 no volume"
        fi
else
        if [ "$TAG_VOL" = 0 ]; then
                VOLS_TAG_0+="$vol "
        else
                VOLS_SEM_SNAP+="$vol "
        fi
fi
done

### SEGUNDA FASE: VERIFICA SE OS VOLUMES QUE ESTAO COM ALGUM PROBLEMA DE SNAPSHOT ESTAO COM TAG 0
        if [ ! -z "$VOLS_TAG_0" ]; then
                for vol_tag_0 in $VOLS_TAG_0; do
                        $ECHO "$vol_tag_0\tSem snapshots\tOK (Tag snapshot=0/Snapshots desligados)"
                done
        fi

### TERCEIRA FASE: CASO TENHA ALGUM VOLUME SEM SNAP, VERIFICA SE TRATA DE UM DISCO DE AUTOSCALING
if [ ! -z "$VOLS_SEM_SNAP" ]; then
        INSTANCES=$($AWS ec2 describe-instances --profile $1 --region $2 --filters "Name=tag-key,Values=aws:autoscaling:groupName" --output json 2>&1) ; if_error describe-instances "$INSTANCES"
        AS_INSTANCES=$(echo "$INSTANCES" | jq -r '.Reservations[] | .Instances[] | .InstanceId' 2>&1) ; if_error var-as-instances "$AS_INSTANCES"
        for as_inst in $AS_INSTANCES; do
        SEARCH_VOLS_AS=$(echo "$VOLUMES" | jq --arg as_inst "$as_inst" -r '[.Volumes[].Attachments[]|select(.InstanceId == $as_inst).VolumeId]' | egrep -v '\[|\]' | sed 's/ //g' | sed 's/\"//g' | sed 's/,//g')
        SEARCH_VOLS_AS=$(echo $SEARCH_VOLS_AS)
        VOLS_AS+="$SEARCH_VOLS_AS "
        done

VOLS_SEM_SNAP=$(echo "$VOLS_SEM_SNAP" | sort)
VOLS_AS=$(echo "$VOLS_AS" | sort)

for vol_as in $VOLS_AS; do
        $ECHO "$vol_as\tSem snapshots\tOK (Volume de Autoscaling Group)"
        VOLS_SEM_SNAP=$(echo $VOLS_SEM_SNAP | sed "s/$vol_as//g")
done
        if [ ! -z "$VOLS_SEM_SNAP" ]; then
                for vol_sem_snap in $VOLS_SEM_SNAP; do
                RESULT=1; log_all "$vol_sem_snap\tSem snapshots\tERRO: (tag snapshot=0 nao definida e nao contem snapshots)"
                done
        fi
fi

log_all "Fim do script - Resultado final: $RESULT"

echo $RESULT
